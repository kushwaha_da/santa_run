﻿public class Constants 
{

	//Game tags
	public const string TAG_SANTA = "Player";
	public const string TAG_ICEBALL = "IceBall";
	public const string TAG_SNOWMAN = "SnowMan";
	public const string TAG_TREE_LEFT_OBS = "TreeLeftObs";
	public const string TAG_TREE_RIGHT_OBS = "TreeRightObs";
	public const string TAG_SNOWPILEOBS = "SnowPileObs";
	public const string TAG_GOLD_GIFTS = "GoldGift";
	public const string TAG_SILVER_GIFTS = "SilverGift";
	public const string TAG_BRONZE_GIFTS = "BronzeGift";
	public const string TAG_REACHED_SCW = "ReachedSCW";
	public const string TAG_SPAWN = "SpawnSCW";
	//Game Constants
	public const float GAME_TIME = 60.0f;

	public const string TESTING_BUILD_API = "http://digitalagents.in/development_auth.php";
	public const string AUTHENTICATION_API = "http://digitalagents.in/validate_santa_run.php?mac=";
	public const string AUTHENTICATION_WITH_NAME_API = "http://digitalagents.in/auth_santa_game.php?mac=";
}
