﻿namespace InputUtility
{
	
	public class Events
	{
		public static string INPUT_RECIEVED = "Input Recieved";
		public static string GAME_EVENT = "Game Event";
	}

	public class InputActions{
		//For input
		public static string SWIPE_LEFT = "Swipe Left";
		public static string SWIPE_RIGHT = "Swipe Right";
		public static string SWIPE_UP = "Swipe Up";
		public static string SWIPE_DOWN = "Swipe Down";
		public static string START = "Start";
		public static string FIRE = "Fire";
	}

	public  static class GameEvents
	{
		//Game events
		public static string START_ANIM = "Start_Anim";
		public static string GAME_START = "Game_Start";
		public static string GAME_RESTART = "Game_ReStart";
		public static string GAME_OVER = "Game_Over";
		public static string GAME_IS_RUNNING = "Running";
	}

}