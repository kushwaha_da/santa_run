﻿using UnityEngine;
using System.Collections;
using ParticlePlayground;

public class RotationActivator : MonoBehaviour {

	// Use this for initialization
	void Start () {

		GetComponent<PlaygroundParticlesC> ().rotationSpeedMin = -250.0f;
		GetComponent<PlaygroundParticlesC> ().rotationSpeedMax = -100.0f;

	}
	

}
