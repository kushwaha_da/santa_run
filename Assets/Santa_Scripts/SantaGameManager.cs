﻿using UnityEngine;
using System.Collections;
using InputUtility;
using System.Net;
using System.Net.NetworkInformation;
using System.Collections.Generic; 
using System.Runtime.Serialization.Formatters.Binary; 
using System.IO;
using System;
using UnityEngine.UI;

public class SantaGameManager : GenericMonoBehaviourSingleton<SantaGameManager> {

	[SerializeField]
	InputField nameField,emailText;

	[SerializeField]
	GameObject ValidationScreen,validateButton;

	string name,emailId;

	private EGameState m_eCurrentState, m_ePrevState;

	bool isAuthenticated = false;

	// Use this for initialization
	void Start () 
	{
		Application.targetFrameRate = 60;
		gameObject.AddComponent<InputReceiver> ();
		Messenger.AddListener<string> (Events.INPUT_RECIEVED, OnInputRecieved);
		Messenger.AddListener<string> (Events.GAME_EVENT, OnGameEventTrigger);

		Messenger.MarkAsPermanent (Events.INPUT_RECIEVED);
		Messenger.MarkAsPermanent (Events.GAME_EVENT);
		LoadGameData ();
		ShowFirstScene ();
	}

	#region Authentication
	string GetMacAddress()
	{
		string macAdress = "";
		NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces ();
		for (int i = 0; i < nics.Length; i++)
		{
			if (nics [i].NetworkInterfaceType == NetworkInterfaceType.Ethernet)
			{
				PhysicalAddress address = nics [i].GetPhysicalAddress ();

				if (address.ToString() != "") 
				{
					Debug.Log ("I - " + i);
					return address.ToString();
				}
			}

		}

		return null;
	}

	public void Activator()
	{
		if (!(nameField.text.Length == 0) && (emailText.text.Contains("@"))) {
			name = nameField.text;
			emailId = emailText.text;

			isAuthenticated = false;
			string macAdr = GetMacAddress ();
			Debug.Log ("Mac adress - " + macAdr);
			string urltoCall = "";

			#if TEST_BUILD
			urltoCall = Constants.TESTING_BUILD_API;
			#else
			urltoCall = Constants.AUTHENTICATION_WITH_NAME_API + macAdr + "&name=" + name + "&email=" + emailId;
			#endif
			WWW www = new WWW (urltoCall);
			StartCoroutine (Authentication (www));
		}

	}

	public void Validator()
	{
		validateButton.GetComponent<Button> ().interactable = false;
		validateButton.GetComponentInChildren<Text>().text ="Waiting for approval..";

		string macAdr = GetMacAddress ();
		Debug.Log ("Mac adress - " + macAdr);
		string urltoCall = "";

		#if TEST_BUILD
		urltoCall = Constants.TESTING_BUILD_API;
		#else
		urltoCall = Constants.AUTHENTICATION_API + macAdr;
		#endif
		WWW www = new WWW (urltoCall);
		StartCoroutine (Authentication (www));
	}

	IEnumerator Authentication(WWW www)
	{
		yield return www;

		if (www.error == null) 
		{
			Debug.Log ("Success Response - " + www.text);
			#if TEST_BUILD
			if (www.text == "1") 
			{
				Debug.Log ("Authenticated");
				isAuthenticated = true;
				SaveGameData ();
				SetState(EGameState.Branding);

			}
			#else
			yield return www;
			Debug.Log("Now u can choose..");
			if (www.text == "validate") 
			{
				Debug.Log ("Authenticated");
				isAuthenticated = true;
				SaveGameData ();
				SetState(EGameState.Branding);
			}

			else
			{
				SetState(EGameState.Validation);
			}
			#endif

		}
		else 
		{
			Debug.Log ("Failed Response - " + www.error);
		}
	}

	public void SaveGameData() 
	{
		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create (Application.persistentDataPath + "/santaDt.gd");

		GameData gd = new GameData ();
		gd.activationTime = DateTime.Now;
		gd.isValid = isAuthenticated;
		Debug.Log ("Save time = " + gd.activationTime);
		Debug.Log ("isAuthenticated = " + gd.isValid);
		bf.Serialize (file, gd);
		file.Close();
	}

	public void LoadGameData()
	{
		if (File.Exists (Application.persistentDataPath + "/santaDt.gd")) 
		{
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/santaDt.gd", FileMode.Open);
			GameData gd = (GameData)bf.Deserialize (file);
			Debug.Log ("Last time " + gd.activationTime);
			file.Close ();

			isAuthenticated = gd.isValid;

			TimeSpan timeSinceActivation = DateTime.Now - gd.activationTime;
			//			if (timeSinceActivation.Days <= 7)
			//			{
			//				authenticated = true;
			//Debug.Log ("Still valid");
			//			} 
			//			else
			//			{
			//				authenticated = false;
			//			}

			//			if(DateTime.Now.d)
		}
	}

	public void SendValidationRequest()
	{
		string macAdr = GetMacAddress ();
		Debug.Log ("Mac adress - " + macAdr);
		string urltoCall = "";

		#if TEST_BUILD
		urltoCall = Constants.TESTING_BUILD_API;
		#else
		urltoCall = Constants.AUTHENTICATION_WITH_NAME_API + macAdr + "&name=" + name + "&email=" + emailId;
		#endif
		WWW www = new WWW (urltoCall);
		StartCoroutine (Authentication (www));
	}
	#endregion


	void OnDestroy()
	{
		Messenger.RemoveListener<string> (Events.INPUT_RECIEVED, OnInputRecieved);
		Messenger.RemoveListener<string> (Events.GAME_EVENT, OnGameEventTrigger);
	}

	void OnInputRecieved(string input)
	{
		if (input.CompareTo (InputActions.START) == 0) 
		{
			switch (m_eCurrentState) 
			{

			case EGameState.Authentication:
				break;

			case EGameState.Validation:
				break;
			
			case EGameState.Branding:
				SetState (EGameState.Instruction);
				break;

			case EGameState.Instruction:
				SetState (EGameState.StartAnim);
				break;

			case EGameState.StartAnim:
				break;

			case EGameState.GamePlay:
				break;

			case EGameState.GameOver:
				SetState (EGameState.Branding);
				break;
			}
		}
	}

	void OnGameEventTrigger(string gameEvent)
	{
		if (gameEvent.CompareTo (GameEvents.START_ANIM) == 0) 
		{
			
		}
		else if (gameEvent.CompareTo (GameEvents.GAME_START) == 0) 
		{
			SetState (EGameState.GamePlay);
		}
		else if (gameEvent.CompareTo (GameEvents.GAME_RESTART) == 0) 
		{
			SetState (EGameState.Branding);
		}
		else if (gameEvent.CompareTo (GameEvents.GAME_OVER) == 0) 
		{
			SetState (EGameState.GameOver);
		}
	}
		
	#region Game State Transitions
	void SetState(EGameState newState)
	{
		if (m_eCurrentState != newState) 
		{
			m_ePrevState = m_eCurrentState;
			m_eCurrentState = newState;
			StateChange ();
		}
	}

	IEnumerator SendRequest()
	{
		yield return new WaitForSeconds (5.0f);
		if (!isAuthenticated) {
			SendValidationRequest ();
		}
		StartCoroutine (SendRequest ());
	}

	void StateChange()
	{
		switch (m_eCurrentState) 
		{

		case EGameState.Authentication:
			Application.LoadLevel ("Authentication");
			break;

		case EGameState.Validation:
			ValidationScreen.SetActive (true);
			if (!isAuthenticated) {
				StartCoroutine (SendRequest ());
			}
			break;

		case EGameState.Branding:
			Application.LoadLevel ("Splash");
			break;

		case EGameState.Instruction:
			Application.LoadLevel ("Start");
			break;

		case EGameState.StartAnim:
			break;

		case EGameState.GamePlay:
			Application.LoadLevel ("GamePlay");
			break;

		case EGameState.GameOver:
			break;
		}
	}
	#endregion

	void ShowFirstScene()
	{
		if (isAuthenticated) 
		{
			SetState (EGameState.Branding);
		} 
		else
		{
			SetState (EGameState.Authentication);
		}
	}
}

public enum EGameState
{
	Authentication =0,
	Validation,
	Branding,
	Instruction,
	StartAnim,
	GamePlay,
	GameOver
}
