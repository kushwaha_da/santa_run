﻿using UnityEngine;
using System.Collections;

public class SpeedIncreaser : MonoBehaviour {
	public float hivalue;
	public float lowvalue;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.I)) {
			Time.timeScale = hivalue;
		} else if (Input.GetKeyDown (KeyCode.U)) {
			Time.timeScale = lowvalue;
		}
		else if (Input.GetKeyDown (KeyCode.O)) {
			Time.timeScale = 1;
		}
	}
}
