﻿using UnityEngine;
using System.Collections;
using ParticlePlayground;

public class HitSnowMan : MonoBehaviour {

	[SerializeField]
	GameObject snowManDeathParticle;

	[SerializeField]
	AudioSource snowManBlast;

	void OnTriggerEnter(Collider collision)
	{
		if (collision.GetComponent<Collider>().CompareTag (Constants.TAG_SNOWMAN)) {
			snowManBlast.Play ();
			ScoreManager.score += 50;
			snowManDeathParticle.GetComponent<ParticleSystem> ().Play ();
			collision.GetComponentInParent<PlaygroundParticlesC> ().enabled = true;
			Destroy (collision.gameObject);
			Destroy (gameObject);
			StartCoroutine(TurnOffParticle(collision));

		}
	}

	IEnumerator TurnOffParticle(Collider collision)
	{	
		yield return new WaitForSeconds (0.05f);
		collision.GetComponentInParent<PlaygroundParticlesC> ().enabled = false;
	}
}
